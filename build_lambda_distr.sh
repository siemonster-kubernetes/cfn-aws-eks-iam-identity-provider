#!/usr/bin/env bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${DIR}"
PKG_NAME="$( basename $DIR)"
PKG_VERSION=v1

docker run --rm -it -v "$(pwd)/build:/build:ro" -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:python3.7
mv lambda/lambda.zip lambda/${PKG_NAME}_${PKG_VERSION}.zip

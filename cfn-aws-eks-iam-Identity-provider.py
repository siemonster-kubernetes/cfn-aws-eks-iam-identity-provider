#!/usr/bin/env python
from crhelper import CfnResource
from botocore.exceptions import ClientError
import boto3
import logging


logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = boto3.client('iam')
except Exception as e:
    helper.init_failure(e)


def get_properties(event):
    allowed_params = ['Url', 'ThumbprintList']
    return {k: v for k, v in event['ResourceProperties'].items() if k in allowed_params}


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment
    client_id_list = {'ClientIDList': ["sts.amazonaws.com"]}
    try:
        properties = get_properties(event)
        response = client.create_open_id_connect_provider(**client_id_list, **properties)
        helper.Data.update({
            'Arn': response['OpenIDConnectProviderArn'],
        })
        return response['OpenIDConnectProviderArn']
    except Exception as e:
        helper.init_failure(e)


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes


@helper.delete
def delete(event, context):
    logger.info("Got Delete")
    logger.info(event)
    if 'PhysicalResourceId' not in event:
        return

    if not event['PhysicalResourceId'].startswith('arn:aws:iam:'):
        return

    response = client.delete_open_id_connect_provider(
        OpenIDConnectProviderArn=event['PhysicalResourceId']
    )


def handler(event, context):
    helper(event, context)
